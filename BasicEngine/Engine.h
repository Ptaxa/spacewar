#pragma once
#include "Core\Init\Init_GLUT.h"
#include "Managers\Scene_Manager.h"
#include "Rendering\Texture\TextureLoader.h"
#include "Managers\GameManager.h"
#include "Managers\ControlModelManager.h"

namespace BasicEngine
{
	class Engine
	{

	public:
		Engine();
		~Engine();

		//OpenGL and manager init
		bool Init();

		//Loop
		void Run();

		//Getters
		Managers::Scene_Manager*  GetScene_Manager()  const;
		Managers::Shader_Manager* GetShader_Manager() const;
		Managers::Models_Manager* GetModels_Manager() const;
		Managers::GameManager* GetGame_Manager() const;
		Managers::ControlModelManager* GetControlModelManager() const;

		TextureLoader* GetTexture_Loader() const;
	private:
		Managers::Scene_Manager*  m_scene_manager;
		Managers::Shader_Manager* m_shader_manager;
		Managers::Models_Manager* m_models_manager;
		Managers::GameManager* m_game_manager;
		Managers::ControlModelManager* m_control_model_manager;
		TextureLoader* m_texture_loader;
	};
}