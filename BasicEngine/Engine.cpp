#include "Engine.h"
using namespace BasicEngine;
using namespace Core;

Engine::Engine()
{

}

TextureLoader* Engine::GetTexture_Loader() const
{
	return m_texture_loader;
}

bool Engine::Init()
{
	WindowInfo window;
	ContextInfo context(4, 4, true);
	FramebufferInfo frameBufferInfo(true, true, true, true);
	WorldSettings worldSettings;
	Init::Init_GLUT::init(window, context, frameBufferInfo, worldSettings);

	m_scene_manager = new Managers::Scene_Manager();
	m_game_manager = new Managers::GameManager(worldSettings);
	IListener* scene = m_scene_manager;
	IGameListener *game = m_game_manager;

	Init::Init_GLUT::setListener(scene);
	Init::Init_GLUT::setGameModel(game);

	m_shader_manager = new Managers::Shader_Manager();
	m_shader_manager->CreateProgram("colorShader",
		"..\\BasicEngine\\Shaders\\Vertex_Shader.glsl",
		"..\\BasicEngine\\Shaders\\Fragment_Shader.glsl");

	if (m_scene_manager && m_shader_manager)
	{
		m_models_manager = new Managers::Models_Manager();
		m_scene_manager->SetModelsManager(m_models_manager);
		m_control_model_manager = new Managers::ControlModelManager(m_models_manager,m_game_manager);
	}
	else
	{
		return false;
	}

	return true;
}

//Create the loop
void Engine::Run()
{
	m_game_manager->startup();
	Init::Init_GLUT::run();
}

Managers::Scene_Manager* Engine::GetScene_Manager() const
{
	return m_scene_manager;
}

Managers::Shader_Manager* Engine::GetShader_Manager() const
{
	return m_shader_manager;
}

Managers::Models_Manager* Engine::GetModels_Manager() const
{
	return m_models_manager;
}

Managers::GameManager* Engine::GetGame_Manager() const
{
	return m_game_manager;
}

Managers::ControlModelManager* Engine::GetControlModelManager() const
{
	return m_control_model_manager;
}

Engine::~Engine()
{
	if (m_scene_manager)
		delete m_scene_manager;
}