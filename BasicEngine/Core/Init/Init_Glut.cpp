#include "Init_GLUT.h"

using namespace BasicEngine::Core::Init;

BasicEngine::Core::IListener* Init_GLUT::listener = NULL;
BasicEngine::Core::WindowInfo Init_GLUT::windowInformation;
BasicEngine::Core::IGameListener* Init_GLUT::game = NULL;
BasicEngine::Core::WorldSettings Init_GLUT::worldSettings;

void Init_GLUT::init(const BasicEngine::Core::WindowInfo& windowInfo,
	const BasicEngine::Core::ContextInfo& contextInfo,
	const BasicEngine::Core::FramebufferInfo& framebufferInfo,
	const BasicEngine::Core::WorldSettings& world)
{
	windowInformation = windowInfo;
	worldSettings = world;

	int fakeargc = 1;
	char *fakeargv[] = { "fake", NULL };
	glutInit(&fakeargc, fakeargv);

	if (contextInfo.core)
	{
		glutInitContextVersion(contextInfo.major_version,
			contextInfo.minor_version);
		glutInitContextProfile(GLUT_CORE_PROFILE);
	}
	else
	{
		glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	}

	glutInitDisplayMode(framebufferInfo.flags);
	glutInitWindowPosition(windowInfo.position_x,
		windowInfo.position_y);
	glutInitWindowSize(windowInfo.width, windowInfo.height);

	glutCreateWindow(windowInfo.name.c_str());
	
	//enable debug
	glEnable(GL_DEBUG_OUTPUT);

	Core::Init::Init_GLEW::Init();
	glDebugMessageCallback(DebugOutput::myCallback, NULL);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE,
		GL_DONT_CARE, 0, NULL, GL_TRUE);
	
	glutIdleFunc(idleCallback);


	glutCloseFunc(closeCallback);
	glutDisplayFunc(displayCallback);
	glutReshapeFunc(reshapeCallback);
	glutTimerFunc(worldSettings.delay, timerFunc, 0);
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE,
		GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	//printOpenGLInfo(windowInfo, contextInfo);
}


void Init_GLUT::run()
{
	glutMainLoop();
}

void Init_GLUT::close()
{
	glutLeaveMainLoop();
}

void Init_GLUT::timerFunc(int value)
{
	game->timer();
	glutTimerFunc(worldSettings.delay, timerFunc, 0);
}

void Init_GLUT::idleCallback(void)
{
	glutPostRedisplay();
}

void Init_GLUT::closeCallback()
{
	close();
}

void Init_GLUT::enterFullscreen()
{
	glutFullScreen();
}

void Init_GLUT::exitFullscreen()
{
	glutLeaveFullScreen();
}

void Init_GLUT::printOpenGLInfo(const BasicEngine::Core::WindowInfo& windowInfo,
	const BasicEngine::Core::ContextInfo& contextInfo) {

	const unsigned char* renderer = glGetString(GL_RENDERER);
	const unsigned char* vendor = glGetString(GL_VENDOR);
	const unsigned char* version = glGetString(GL_VERSION);

	std::cout << "******************************************************               ************************" << std::endl;
	std::cout << "GLUT:Initialise" << std::endl;
	std::cout << "GLUT:\tVendor : " << vendor << std::endl;
	std::cout << "GLUT:\tRenderer : " << renderer << std::endl;
	std::cout << "GLUT:\tOpenGl version: " << version << std::endl;
}

void Init_GLUT::displayCallback()
{
  if (listener)
  {
    listener->notifyBeginFrame();
    listener->notifyDisplayFrame();
 
    glutSwapBuffers();
 
    listener->notifyEndFrame();
  }
}
 
void Init_GLUT::reshapeCallback(int width, int height)
{
  if (windowInformation.isReshapable == true)
  {
	  if (listener)
	  {
		  listener->notifyReshape(width,
			  height,
			  windowInformation.width,
			  windowInformation.height);
	  }
     windowInformation.width = width;
     windowInformation.height = height;
  }
  else
  {
	  glutReshapeWindow(windowInformation.width, windowInformation.height);
	  if (listener)
	  {
		  listener->notifyReshape(
			  windowInformation.width,
			  windowInformation.height,
			  windowInformation.width,
			  windowInformation.height);
	  }
  }

}
 
void Init_GLUT::setListener(BasicEngine::Core::IListener*& iListener)
{
	listener = iListener;
}

void Init_GLUT::setGameModel(BasicEngine::Core::IGameListener*& gameModel)
{
	game = gameModel;
}