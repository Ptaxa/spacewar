#pragma once
#include "ContextInfo.h"
#include "FrameBufferInfo.h"
#include "WindowInfo.h"
#include <iostream>
#include "Init_GLEW.h"
#include "IListener.h"
#include "DebugOutput.h"
#include "IGameListener.h"
#include "WorldSettings.h"
//#include ""
namespace BasicEngine {
	namespace Core {
		namespace Init {

			class Init_GLUT {

			public:             
				static void init(const BasicEngine::Core::WindowInfo& window,
					const BasicEngine::Core::ContextInfo& context,
					const BasicEngine::Core::FramebufferInfo& framebufferInfo,
					const BasicEngine::Core::WorldSettings& worldSettings);

			public:
				static void run();
				static void close();

				void enterFullscreen();
				void exitFullscreen();
				
				static void printOpenGLInfo(const BasicEngine::Core::WindowInfo& windowInfo,
					const BasicEngine::Core::ContextInfo& context);

				static void setListener(BasicEngine::Core::IListener*& iListener);
				static void setGameModel(BasicEngine::Core::IGameListener*& gameModel);
			private:
				static void idleCallback(void);
				static void timerFunc(int);
				static void displayCallback(void);
				static void reshapeCallback(int width, int height);
				static void closeCallback();
				static BasicEngine::Core::IListener* listener;
				static BasicEngine::Core::WindowInfo windowInformation;
				static BasicEngine::Core::IGameListener* game;
				static BasicEngine::Core::WorldSettings worldSettings;

			};
		}
	}
}