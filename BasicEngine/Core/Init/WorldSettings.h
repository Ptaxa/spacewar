#pragma once

#include <string>
namespace BasicEngine {
	namespace Core
	{
		struct WorldSettings
		{
			double minX;
			double minY;
			double maxX;
			double maxY;
			double dX;
			double dY;

			const double delay = 1;
			//this settings are calculated for current worldMatrix and resolution
			WorldSettings()
			{
				maxX = 9.5;
				minX = -maxX;
				maxY = 7;
				minY = -maxY;
				dX = maxX - minX;
				dY = maxY - minY;
			}

			WorldSettings(const WorldSettings& worldSettings)
			{
				this->minX = worldSettings.minX;
				this->maxX = worldSettings.maxX;
				this->minY = worldSettings.minY;
				this->maxY = worldSettings.maxY;
				this->dX = worldSettings.dX;
				this->dY = worldSettings.dY;
			}

			void operator=(const WorldSettings& worldSettings)
			{
				this->minX = worldSettings.minX;
				this->maxX = worldSettings.maxX;
				this->minY = worldSettings.minY;
				this->maxY = worldSettings.maxY;
				this->dX = worldSettings.dX;
				this->dY = worldSettings.dY;;
			}

		};
	}
}