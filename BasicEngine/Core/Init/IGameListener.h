#pragma once
namespace BasicEngine {
	namespace Core {

			class IGameListener {
			public:
				virtual void timer() = 0;
			private:

			};
	}
}