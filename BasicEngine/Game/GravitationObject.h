#pragma once
#include "position.h"

namespace BasicEngine {
	namespace Game {
			class GravitationObject
			{
			public:
				GravitationObject();
				GravitationObject(double);
				virtual ~GravitationObject();
				virtual position* getPosition() = 0;
				BasicEngine::Game::position getAttractionVector(position*);
				bool attracts();
			private:
				double gravitation;
				static constexpr double NO_GRAVITATION_RADIUS = 3;
				static constexpr double GRAVITATION_POWER = 1000;
			};
	}
}