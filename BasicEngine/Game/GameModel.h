#pragma once

#include "MovingGravitationObject.h"
#include "..\Rendering\Models\Model.h"

namespace BasicEngine {
	namespace Game {
		class GameModel : public BasicEngine::Game::MovingGravitationObject, public BasicEngine::Rendering::Models::Model
		{
		public:
			GameModel();
			GameModel(double gravitation);
			std::string getName();
			virtual position* getPosition() override;
			virtual bool isDestroyed() override;
			virtual void setStartPosition(position *pos) override;
		private:
			position *currentPosition = nullptr;
			std::string name = "";
		};
	}
}