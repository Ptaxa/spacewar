#pragma once
#include "MovingObject.h"
#include "GravitationObject.h"

namespace BasicEngine {
	namespace Game {
			class MovingGravitationObject : public MovingObject, public GravitationObject
			{
			public:
				MovingGravitationObject();
				MovingGravitationObject(double gravitation);
				virtual ~MovingGravitationObject();
				virtual position* getPosition() = 0;
				virtual void setStartPosition(position *pos) = 0;
				virtual bool isDestroyed() = 0;
				void destroy();
			protected:
				bool destroyed = false;
			};
	}
}