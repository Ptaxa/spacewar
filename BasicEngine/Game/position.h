#pragma once
#include <cmath>

namespace BasicEngine {
	namespace Game {
		struct position
		{
			float x;
			float y;
			bool isVector;

			position()
			{
				x = 0;
				y = 0;
				isVector = false;
			}
			
			position(double x, double y, bool isVector = false)
			{
				this->x = x;
				this->y = y;
				this->isVector = isVector;
			}

			position(const position& position)
			{
				x = position.x;
				y = position.y;
				isVector = position.isVector;
			}

			void operator=(const position& position)
			{
				x = position.x;
				y = position.y;
				isVector = position.isVector;
			}

			const position operator*(const position& right) const 
			{
				return position(sqrt(pow(x, 2) + pow(right.x, 2)),
							sqrt(pow(y, 2) + pow(right.y, 2)),
							true);
								
			}

			const position operator-(const position& right) const
			{
				return position(x - right.x, y - right.y, true);
			}
		};
	}
}