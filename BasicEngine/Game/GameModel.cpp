#include "GameModel.h"

using namespace BasicEngine::Game;

GameModel::GameModel()
{

}

GameModel::GameModel(double gravitation = 0) : MovingGravitationObject(gravitation)
{
}

bool GameModel::isDestroyed()
{
	return destroyed;
}

void GameModel::setStartPosition(position *pos)
{
	if (!currentPosition)
	{
		currentPosition = pos;
	}
}

std::string GameModel::getName()
{
	return name;
}

position* GameModel::getPosition()
{
	return currentPosition;
}
