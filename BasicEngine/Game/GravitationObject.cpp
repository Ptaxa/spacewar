#include "GravitationObject.h"

using namespace BasicEngine::Game;

GravitationObject::GravitationObject()
{

}

GravitationObject::GravitationObject(double gravitation = 0)
{
	this->gravitation = gravitation;
}

GravitationObject::~GravitationObject()
{

}

position GravitationObject::getAttractionVector(position *point)
{
	position *objectPosition = getPosition();
	position distanceVector = *objectPosition - *point;
	
	double distance = sqrt(pow(distanceVector.x, 2) + pow(distanceVector.y, 2));
	if (distance > NO_GRAVITATION_RADIUS)
	{
		return position(0, 0, true);
	}
	
	return position(distanceVector.x * gravitation / distance / GRAVITATION_POWER, distanceVector.y * gravitation / distance / GRAVITATION_POWER, true);
}

bool GravitationObject::attracts()
{
	return gravitation != 0;
}