#pragma once
#include "position.h"

namespace BasicEngine{
	namespace Game{
			class MovingObject
			{
			public:
				//MovingObject();
				MovingObject();
				MovingObject(position *vector);
				virtual ~MovingObject();
				virtual position* getPosition() = 0;
				//summ all directions
				void turn(position *direction);
				void move();

			private:
				position *directionVector = nullptr;
				bool tooFar = false;
				const double maxSpeed = 0.5;
			};
	}
}