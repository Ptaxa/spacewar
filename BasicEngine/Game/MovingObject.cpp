#include <cmath>
#include "MovingObject.h"


using namespace BasicEngine::Game;

MovingObject::MovingObject()
{
	this->directionVector = new position(0, 0, true);
}

MovingObject::MovingObject(position *vector)
{
	this->directionVector = vector;
}

MovingObject::~MovingObject()
{

}

void MovingObject::move()
{
	position *currentPosition = getPosition();
	currentPosition->x += directionVector->x;
	currentPosition->y += directionVector->y;
	
}

void MovingObject::turn(position *direction)
{
	position newVector = *directionVector * *direction;
	//check for max speed later

	*directionVector = newVector;

}