#include "MovingGravitationObject.h"

using namespace BasicEngine::Game;

MovingGravitationObject::MovingGravitationObject()
{

}

MovingGravitationObject::MovingGravitationObject(double gravitation) : GravitationObject(gravitation)
{

}


MovingGravitationObject::~MovingGravitationObject()
{

}

void MovingGravitationObject::destroy()
{
	destroyed = true;
}
