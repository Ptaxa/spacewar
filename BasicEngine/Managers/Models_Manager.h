#pragma once
#include <map>
#include <list>
#include "Shader_Manager.h"
#include "../Rendering/IGameObject.h"
#include "../Rendering/Models/Triangle.h"
#include "../Rendering/Models/Quad.h"
//#include "../Rendering/Models/Cube.h"

using namespace BasicEngine::Rendering;
namespace BasicEngine
{

	namespace Managers
	{
		class Models_Manager
		{
		public:
			Models_Manager();
			~Models_Manager();

			void Draw();
			void Draw(const glm::mat4& projection_matrix,
				const glm::mat4& view_matrix);

			void Update();
			void DeleteModel(const std::string& gameModelName);
			void SetModel(const std::string& gameObjectName, IGameObject* gameObject);
			void SetModel(IGameObject* gameObject);
			void addBullet(IGameObject *object);
			const IGameObject& GetModel(const std::string& gameModelName) const;

		private:
			//void deleteBullets();

			std::map<std::string, IGameObject*> gameModelList;
			std::list<IGameObject*> bullets;
			IGameObject *backGround;
		};

	}
}