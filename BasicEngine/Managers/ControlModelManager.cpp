#include "ControlModelManager.h"

using namespace BasicEngine::Managers;

BasicEngine::Managers::Models_Manager* ControlModelManager::m_models_manager = nullptr;
BasicEngine::Managers::GameManager* ControlModelManager::m_game_manager = nullptr;


ControlModelManager::ControlModelManager(Models_Manager *modelManager, GameManager *gameManager)
{
	this->m_models_manager = modelManager;
	this->m_game_manager = gameManager;
}

void ControlModelManager::setModel(const std::string& objectName, Rendering::Models::Model* object)
{
	m_models_manager->SetModel(objectName, object);
}

void ControlModelManager::setPlanet(const std::string& objectName, Game::GameModel* object)
{
	m_models_manager->SetModel(objectName, object);
	m_game_manager->setPlanet(objectName, object);
}

void ControlModelManager::setPlayer(const std::string& objectName, Game::GameModel* object)
{
	m_models_manager->SetModel(objectName, object);
	m_game_manager->setPlayer(objectName, object);
}
