#include "Models_Manager.h"

using namespace BasicEngine::Managers;
using namespace BasicEngine::Rendering;

Models_Manager::Models_Manager()
{
	
}

Models_Manager::~Models_Manager()
{
	//auto -it's a map iterator
	for (auto model : gameModelList)
	{
		delete model.second;
	}
	gameModelList.clear();
}

void Models_Manager::DeleteModel(const std::string& gameModelName)
{
	IGameObject* model = gameModelList[gameModelName];
	model->Destroy();
	gameModelList.erase(gameModelName);
}

void Models_Manager::addBullet(IGameObject *bullet)
{
	bullets.push_back(bullet);
}

const IGameObject& Models_Manager::GetModel(const std::string& gameModelName) const
{
	return (*gameModelList.at(gameModelName));
}

void Models_Manager::Update()
{
	for (auto model : gameModelList)
	{
		model.second->Update();
	}

	std::list<IGameObject*> toDelete;
	for (auto model : bullets)
	{
		if (model->isDestroyed())
			toDelete.push_back(model);
	}
	
	for (auto model : toDelete)
	{
		toDelete.remove(model);
		delete model;
	}
}

//backGround has to be drawn first
void Models_Manager::Draw()
{
	if (backGround)
	backGround->Draw();
	for (auto model : gameModelList)
	{
		model.second->Draw();
	}

	for (auto model : bullets)
	{
		model->Draw();
	}
}

void Models_Manager::Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix)
{
	if (backGround)
	backGround->Draw(projection_matrix, view_matrix);
	for (auto model : gameModelList)
	{
		model.second->Draw(projection_matrix, view_matrix);
	}
	for (auto model : bullets)
	{
		model->Draw(projection_matrix, view_matrix);
	}
}

void Models_Manager::SetModel(const std::string& gameObjectName, IGameObject* gameObject)
{
	if (gameObjectName == "backGround") 
	{
		backGround = gameObject;
	}
	else
	{
		gameModelList[gameObjectName.c_str()] = gameObject;
	}
	
}

void Models_Manager::SetModel(IGameObject* gameObject)
{
	bullets.push_back(gameObject);
}
