#pragma once
#include <map>
#include <list>
#include "../Core/Init/IGameListener.h"
//#include "ControlModelManager.h"
//#include "../Rendering/IGameModel.h"
#include "../Game/MovingGravitationObject.h"
#include "../Core/Init/WorldSettings.h"

using namespace BasicEngine;

namespace BasicEngine{
	namespace Managers{
		class GameManager : public Core::IGameListener
		{
		public:
			GameManager(Core::WorldSettings &worldSettings);
			~GameManager();
			virtual void timer() override;
			void startup();
			void setPlanet(const std::string& objectName, Game::MovingGravitationObject* object);
			void setPlayer(const std::string& objectName, Game::MovingGravitationObject* object);

			//void addBullet(Game::MovingGravitationObject* MovingGravitationObject);

			//void SetModel(const std::string& GameModelName, Game::GameModel* GameModel);
			//void SetModel(Game::GameModel* GameModel);
		private:
			void attraction();
			void moveObjects();
			void searchDestroyedObjects();
			bool hitBoxIntersects(Game::position *first, double r1,
				Game::position *second, double r2);
			//std::map<std::string, Rendering::IGameModel*> gameModelList;
			std::map<std::string, Game::MovingGravitationObject*> players;
			std::map<std::string, Game::MovingGravitationObject*> gravitationalObjects;
			std::list<Game::MovingGravitationObject*> bullets;
			Core::WorldSettings worldSettings;
			//ControlModelManager *controlManager;
		};

	}
}