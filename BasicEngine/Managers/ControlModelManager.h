#pragma once
#include "Models_Manager.h"
#include "GameManager.h"
#include "..\Game\GameModel.h"

using namespace BasicEngine;
namespace BasicEngine
{
	namespace Managers
	{
		class ControlModelManager
		{
		public:
			ControlModelManager(Models_Manager *modelManager, GameManager *gameManager);
			void setModel(const std::string& objectName, Rendering::Models::Model* object);
			
			void setPlanet(const std::string& objectName, Game::GameModel* object);
			void setPlayer(const std::string& objectName, Game::GameModel* object);
			
			//static void addBullet(Game::GameModel* MovingGravitationObject);

		private:
			static Models_Manager *m_models_manager;
			static Managers::GameManager *m_game_manager;
		};
	}
}