#pragma once
#include "glew/glew.h"
#include "freeglut/freeglut.h"
#include <iostream>
#include <fstream>
#include <map>
#include <vector> 
namespace BasicEngine
{
	namespace Managers
	{

		class Shader_Manager
		{
		public:

			Shader_Manager(void);
			~Shader_Manager(void);

			void CreateProgram(const std::string& shaderName,
				const std::string& vertexShaderFilename,
				const std::string& fragmentShaderFilename);

			static const GLuint GetShader(const std::string&);

		private:
			static std::map<std::string, GLuint> programs;

			std::string ReadShader(const std::string& filename);
			GLuint CreateShader(GLenum shaderType,
				const std::string& source,
				const std::string& shaderName);


		};
	}
}