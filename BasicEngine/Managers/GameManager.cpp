#include "GameManager.h"
#include <time.h>

using namespace BasicEngine::Managers;
GameManager::GameManager(Core::WorldSettings & worldSettings)
{
	this->worldSettings = worldSettings;
}

GameManager::~GameManager()
{

}

//works bad
void GameManager::timer()
{
	attraction();
	//moveObjects();
	//searchDestroyedObjects();
}

void GameManager::startup()
{
	srand(time(NULL));
	double x;
	double y;
	for (auto player : players)
	{
		x = (rand() % 1000) / 100.0 - 5;
		y = (rand() % 800) / 100.0 - 4;
		player.second->setStartPosition(new BasicEngine::Game::position(x, y));
	}
}

void GameManager::attraction() {
	for (auto model : gravitationalObjects)
	{
		if (model.second->attracts())
		{
			for (auto player : players)
			{
				player.second->turn(&(model.second->getAttractionVector(
					player.second->getPosition())) );
			}
			for (auto bullet : bullets)
			{
				bullet->turn(&(model.second->getAttractionVector(
					bullet->getPosition())) );
			}
		}
	}
}

void GameManager::moveObjects() {
	for (auto player : players)
	{
		player.second->move();

	}

	for (auto bullet : bullets)
	{
		bullet->move();
	}
}

//player is destroyed near bullet | planet
//bullet is destroyed near player | plabet

void GameManager::searchDestroyedObjects() 
{
	std::list<std::string> destroyed;
	for (auto player : players)
	{
		for (auto planet : gravitationalObjects)
		{
			if (hitBoxIntersects(player.second->getPosition(), 0.3,
				planet.second->getPosition(), 1))
			{
				player.second->destroy();
				destroyed.push_back(player.first);
			}
		}
	}

	for (auto player : destroyed)
	{
		players.erase(player);
	}

}

//radious is temprorarily
bool GameManager::hitBoxIntersects(Game::position *first, double r1,
	Game::position *second, double r2)
{
	Game::position distanceVector = *second - *first;
	double distance = sqrt(pow(distanceVector.x, 2) + pow(distanceVector.y, 2));
	return distance < (r1 + r2);
}

void GameManager::setPlanet(const std::string& objectName, Game::MovingGravitationObject* object)
{
	gravitationalObjects[objectName.c_str()] = object;
}

void GameManager::setPlayer(const std::string& objectName, Game::MovingGravitationObject* object)
{
	players[objectName.c_str()] = object;
}