#pragma once
#include <BasicEngine\Engine.h>
#include "Models\Planet\Solar.h"
#include "Models\Player\Player.h"
#include "Models\backGround.h"
using namespace BasicEngine;

void downloadShaders(Engine *engine)
{
	engine->GetShader_Manager()->CreateProgram("modelShader",
		"Shaders\\Solar\\Vertex_Shader.glsl",
		"Shaders\\Solar\\Fragment_Shader.glsl");
	engine->GetShader_Manager()->CreateProgram("BackGroundShader",
		"Shaders\\BackGround\\Vertex_Shader.glsl",
		"Shaders\\BackGround\\Fragment_Shader.glsl");
	engine->GetShader_Manager()->CreateProgram("PlayerShader",
		"Shaders\\Player\\Vertex_Shader.glsl",
		"Shaders\\Player\\Fragment_Shader.glsl");
}

void createBackGround(const Engine *engine)
{
	int program;
	unsigned int texture;
	BackGround *backGround = new BackGround();
	program = engine->GetShader_Manager()->GetShader("BackGroundShader");
	backGround->SetProgram(program);


	texture = engine->GetTexture_Loader()->LoadTexture("Textures\\stars.bmp", 640, 480);
	backGround->SetTexture("BackGround", texture);

	backGround->Create();
	engine->GetControlModelManager()->setModel("backGround", backGround);
}

void createSolar(const Engine * engine)
{
	int program;
	unsigned int texture;
	Solar *solar = new Solar(4);
	program = engine->GetShader_Manager()->GetShader("modelShader");
	solar->SetProgram(program);
	solar->Create();
	texture = engine->GetTexture_Loader()->LoadTexture("Textures\\Solar.bmp", 1600, 900);
	solar->SetTexture("Solar", texture);

	engine->GetControlModelManager()->setPlanet("Solar", solar);
}


void addPlayer(const Engine * engine, const std::string name, const std::string texturePath, int textureWidth, int textureHeight)
{
	int program;
	unsigned int texture;
	Player *player = new Player();
	player->setName(name);
	program = engine->GetShader_Manager()->GetShader("PlayerShader");
	player->SetProgram(program);
	player->Create();
	texture = engine->GetTexture_Loader()->LoadTexture(texturePath.c_str(), textureWidth, textureHeight);
	player->SetTexture(name, texture);

	engine->GetControlModelManager()->setPlayer(name, player);
}

int main(int argc, char **argv)
{
	#ifndef DEBUG
		ShowWindow(GetConsoleWindow(), SW_HIDE);
	#endif 
	
	
	Engine* engine = new Engine();
	engine->Init();

	downloadShaders(engine);
	createBackGround(engine);
	createSolar(engine);
	addPlayer(engine, "Player1", "Textures\\player1.bmp", 47, 83);
	addPlayer(engine, "Player2", "Textures\\player2.bmp", 35, 105);

	engine->Run();

	delete engine;
	return 0;
}