#version 430 core
layout(location = 0) out vec4 out_color;

uniform sampler2D texture1;

in vec2 texcoord;

void main(void)
{
  vec4 color = texture(texture1, texcoord);
  out_color = color;
  if ((color[0] + color[1] + color[2]) < 0.1) 
  {
  	out_color[3] = 0.0;
  }
}