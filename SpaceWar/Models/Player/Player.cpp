#pragma once
#include "Player.h"
using namespace BasicEngine::Rendering;

#define PI 3.14159265

Player::Player()
{
}

Player::Player(double gravitation) : GameModel(gravitation)
{

}

Player::~Player()
{
}

void Player::Create()
{
	GLuint vao;
	GLuint vbo;
	GLuint ibo;

	time(&timer);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	std::vector<VertexFormat> vertices;
	std::vector<unsigned int>  indices = { 0,  1,  2,  0,  2,  3 };

	vertices.push_back(VertexFormat(glm::vec3(-0.3, -0.5, -1.0),
		glm::vec2(0, 0)));
	vertices.push_back(VertexFormat(glm::vec3(0.3, -0.5, -1.0),
		glm::vec2(1, 0)));
	vertices.push_back(VertexFormat(glm::vec3(0.3, 0.5, -1.0),
		glm::vec2(1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-0.3, 0.5, -1.0),
		glm::vec2(0, 1)));


	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER,
		vertices.size() * sizeof(VertexFormat),
		&vertices[0],
		GL_STATIC_DRAW);

	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		indices.size() * sizeof(unsigned int),
		&indices[0],
		GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
		sizeof(VertexFormat),
		(void*)(offsetof(VertexFormat, VertexFormat::texture)));
	glBindVertexArray(0);
	this->vao = vao;
	this->vbos.push_back(vbo);
	this->vbos.push_back(ibo);

	rotation_speed = glm::vec3(0.0, 0.0, 0.0);
	rotation = glm::vec3(0.0, 0.0, 0.0);
}

void Player::Update()
{
	rotation = 0.01f * rotation_speed + rotation;
	rotation_sin = glm::vec3(rotation.x * PI / 180, rotation.y * PI / 180, rotation.z * PI / 180);
}

void Player::Draw(const glm::mat4& projection_matrix,
	const glm::mat4& view_matrix)
{
	//glEnable(GL_ALPHA_TEST);
	glUseProgram(program);
	glBindVertexArray(vao);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture(this->playerName));
	unsigned int textureLocation = glGetUniformLocation(program, "texture1");
	glUniform1i(textureLocation, 0);
	glUniform2f(glGetUniformLocation(program, "translation"),
		getPosition()->x, getPosition()->y);
	glUniform3f(glGetUniformLocation(program, "rotation"),
		rotation_sin.x, rotation_sin.y, rotation_sin.z);
	glUniformMatrix4fv(glGetUniformLocation(program, "view_matrix"),
		1, false, &view_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(program, "projection_matrix"),
		1, false, &projection_matrix[0][0]);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void Player::setName(std::string name)
{
	this->playerName = name;
}