#pragma once

//#include <BasicEngine\Rendering\Models\Model.h>
#include <BasicEngine\Game\GameModel.h>
#include <time.h>
#include <stdarg.h>

using namespace BasicEngine::Game;
class Player : public GameModel
{
public:
	Player();
	Player(double gravitaion);
	~Player();
	void setName(std::string name);
	void Create();
	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix) override final;
	virtual void Update() override final;

private:
	std::string playerName;
	glm::vec3 rotation, rotation_speed;
	glm::vec3 rotation_sin;
	time_t timer;
};
