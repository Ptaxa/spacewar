#pragma once

#include <BasicEngine\Rendering\Models\Model.h>
#include <time.h>
#include <stdarg.h>

using namespace BasicEngine::Rendering::Models;
class BackGround : public Model
{
public:
	BackGround();
	~BackGround();

	void Create();
	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix) override final;
	virtual void Update() override final;

private:
	glm::vec3 rotation;
	glm::vec3 rotation_sin;
	GLuint tex_lut;
	//time_t timer;
};
